import { State, EventActions, ADD, REDUCE_MINUTE, REMOVE } from './types'

export function eventReducer(
  state: State = { events: [] },
  action: EventActions,
): State {
  switch (action.type) {
    case ADD:
      return {
        events: [...state.events, action.payload],
      }
    case REDUCE_MINUTE:
      const id = action.payload
      const events = [...state.events]
      const eventIndex = events.findIndex((i) => i.id === id)

      if (eventIndex !== -1) {
        const oldEvent = events[eventIndex]
        events[eventIndex] = {
          ...oldEvent,
          remaining: oldEvent.remaining - 1,
        }

        return {
          events,
        }
      } else {
        return state
      }
    case REMOVE:
      return {
        events: state.events.filter((i) => i.id !== action.payload),
      }
    default:
      return state
  }
}
