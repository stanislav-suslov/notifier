import { eventReducer } from './reducers'
import { EventActions, ADD, Event, REDUCE_MINUTE, REMOVE, State } from './types'

describe('редьюсер событий', () => {
  it('добавляет событие в список', () => {
    const initial: State = {
      events: [
        {
          id: 1,
          remaining: 2,
          name: '3',
        },
      ],
    }

    const payload: Event = {
      id: 2,
      remaining: 3,
      name: '4',
    }

    const action: EventActions = {
      type: ADD,
      payload,
    }

    const newState = eventReducer(initial, action)

    expect(newState.events[1]).toStrictEqual(payload)
  })

  it('уменьшает время для события', () => {
    const initial: State = {
      events: [
        {
          id: 1,
          remaining: 2,
          name: '3',
        },
      ],
    }

    const action: EventActions = {
      type: REDUCE_MINUTE,
      payload: 1,
    }

    const newState = eventReducer(initial, action)

    expect(newState.events[0].remaining).toBe(1)
  })

  it('удаляет событие', () => {
    const initial: State = {
      events: [
        {
          id: 1,
          remaining: 2,
          name: '3',
        },
      ],
    }

    const action: EventActions = {
      type: REMOVE,
      payload: 1,
    }

    const newState = eventReducer(initial, action)

    expect(newState.events).toHaveLength(0)
  })
})
