import {
  ADD,
  ReduceMinuteAction,
  Event,
  REMOVE,
  REDUCE_MINUTE,
  AddAction,
  RemoveAction,
  EventId,
} from './types'

export function Add(event: Event): AddAction {
  return {
    type: ADD,
    payload: event,
  }
}

export function ReduceMinute(eventId: EventId): ReduceMinuteAction {
  return {
    type: REDUCE_MINUTE,
    payload: eventId,
  }
}

export function Remove(eventId: EventId): RemoveAction {
  return {
    type: REMOVE,
    payload: eventId,
  }
}
