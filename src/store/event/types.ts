export type EventId = number

export interface Event {
  id: EventId
  name: string
  remaining: number
}

export interface State {
  events: Event[]
}

export const ADD = 'ADD'
export const REDUCE_MINUTE = 'REDUCE_MINUTE'
export const REMOVE = 'REMOVE'

export interface AddAction {
  type: typeof ADD
  payload: Event
}

export interface ReduceMinuteAction {
  type: typeof REDUCE_MINUTE
  payload: EventId
}

export interface RemoveAction {
  type: typeof REMOVE
  payload: EventId
}

export type EventActions = AddAction | ReduceMinuteAction | RemoveAction
