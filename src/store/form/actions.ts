import {
  Name,
  Remaining,
  SetNameAction,
  SetRemainingAction,
  SET_NAME,
  SET_REMAINING,
} from './types'

export function setName(name: Name): SetNameAction {
  return {
    type: SET_NAME,
    payload: name,
  }
}

export function setRemaining(remaining: Remaining): SetRemainingAction {
  return {
    type: SET_REMAINING,
    payload: remaining,
  }
}
