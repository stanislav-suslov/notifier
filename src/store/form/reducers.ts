import { State, FormActions, SET_NAME, SET_REMAINING } from './types'

const initialState: State = {
  name: '',
  remaining: '',
}

export function formReducer(
  state: State = { ...initialState },
  action: FormActions,
): State {
  switch (action.type) {
    case SET_NAME:
      return {
        ...state,
        name: action.payload,
      }
    case SET_REMAINING:
      return {
        ...state,
        remaining: action.payload,
      }
    case 'RESET': {
      return { ...initialState }
    }
    default:
      return state
  }
}
