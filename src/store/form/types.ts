export type Name = string
export type Remaining = string

export interface State {
  name: Name
  remaining: Remaining
}

export const SET_NAME = 'SET_NAME'
export const SET_REMAINING = 'SET_REMAINING'
export const RESET = 'RESET'

export interface SetNameAction {
  type: typeof SET_NAME
  payload: Name
}

export interface SetRemainingAction {
  type: typeof SET_REMAINING
  payload: Remaining
}

export interface ResetAction {
  type: typeof RESET
}

export type FormActions = SetNameAction | SetRemainingAction | ResetAction
