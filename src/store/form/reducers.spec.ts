import { formReducer } from './reducers'
import { FormActions, State } from './types'

describe('редьюсер формы', () => {
  it('изменяет свойство name', () => {
    const initial: State = {
      name: '',
      remaining: '',
    }
    const action: FormActions = {
      type: 'SET_NAME',
      payload: 'test name',
    }
    const expected: State = {
      name: 'test name',
      remaining: '',
    }

    const newState = formReducer(initial, action)

    expect(newState).toStrictEqual(expected)
  })

  it('изменяет свойство remaining', () => {
    const initial: State = {
      name: '',
      remaining: '',
    }
    const action: FormActions = {
      type: 'SET_REMAINING',
      payload: 'test remaining',
    }
    const expected: State = {
      name: '',
      remaining: 'test remaining',
    }

    const newState = formReducer(initial, action)

    expect(newState).toStrictEqual(expected)
  })

  it('сбрасывает состояние', () => {
    const initial: State = {
      name: 'test name',
      remaining: 'test remaining',
    }
    const action: FormActions = {
      type: 'RESET',
    }
    const expected: State = {
      name: '',
      remaining: '',
    }

    const newState = formReducer(initial, action)

    expect(newState).toStrictEqual(expected)
  })
})
