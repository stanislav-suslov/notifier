import { combineReducers } from 'redux'
import { eventReducer } from './event/reducers'

import { formReducer } from './form/reducers'

export const rootReducer = combineReducers({
  form: formReducer,
  event: eventReducer,
})

export type RootState = ReturnType<typeof rootReducer>
