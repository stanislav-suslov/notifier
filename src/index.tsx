import React from 'react'
import ReactDOM from 'react-dom'
import './assets/styles/index.scss'
import { PageIndex } from './pages/index/index'
import reportWebVitals from './reportWebVitals'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { rootReducer } from '../src/store/index'

const store = createStore(
  rootReducer,
  // @ts-ignore-next-line
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
)

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PageIndex />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
