import React, { FunctionComponent } from 'react'
import { Container } from '../../components/common/container'
import { Reminder } from '../../components/presenters/reminder'

export const PageIndex: FunctionComponent = () => {
  return (
    <div>
      <Container>
        <h1>Reminder</h1>
        <Reminder />
      </Container>
    </div>
  )
}
