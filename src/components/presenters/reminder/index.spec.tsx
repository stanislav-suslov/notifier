import React from 'react'
import { mount, ReactWrapper, HTMLAttributes } from 'enzyme'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { Reminder } from '.'
import { rootReducer } from '../../../store'

function getInputName(wrapper: ReactWrapper): ReactWrapper<HTMLAttributes> {
  return wrapper.find('input[type="text"]')
}

function getInputRemaining(
  wrapper: ReactWrapper,
): ReactWrapper<HTMLAttributes> {
  return wrapper.find('input[type="datetime-local"]')
}

function addEvent(wrapper: ReactWrapper, name: string, remaining: string) {
  getInputName(wrapper).simulate('change', { target: { value: name } })
  getInputRemaining(wrapper).simulate('change', {
    target: { value: remaining },
  })

  wrapper.find('form').simulate('submit')
}

describe('модуль напоминаний', () => {
  it('изначально не содержит событий', () => {
    const store = createStore(rootReducer)
    const wrapper = mount(
      <Provider store={store}>
        <Reminder />
      </Provider>,
    )

    expect(wrapper.find('.event')).toHaveLength(0)

    // Ниже проверка первого expect на корректное определение класса
    addEvent(wrapper, 'testEvent', '2030-01-01T10:30')
    expect(wrapper.find('.event')).toHaveLength(1)
  })

  it('отображает событие после добавления через форму', () => {
    const store = createStore(rootReducer)
    const wrapper = mount(
      <Provider store={store}>
        <Reminder />
      </Provider>,
    )
    const eventName = 'testEvent'
    const eventRemaining = '2030-01-01T10:30'

    expect(wrapper.contains(eventName)).toBeFalsy()

    addEvent(wrapper, eventName, eventRemaining)

    expect(wrapper.contains(eventName)).toBeTruthy()
  })

  it('при добавлении события возвращает состояние формы на исходное значение', () => {
    const store = createStore(rootReducer)
    const wrapper = mount(
      <Provider store={store}>
        <Reminder />
      </Provider>,
    )
    const eventName = 'testEvent'
    const eventRemaining = '2030-01-01T10:30'

    getInputName(wrapper).simulate('change', { target: { value: eventName } })
    getInputRemaining(wrapper).simulate('change', {
      target: { value: eventRemaining },
    })

    expect(getInputName(wrapper).prop('value')).toBe(eventName)
    expect(getInputRemaining(wrapper).prop('value')).toBe(eventRemaining)

    wrapper.find('form').simulate('submit')

    expect(getInputName(wrapper).prop('value')).toBe('')
    expect(getInputRemaining(wrapper).prop('value')).toBe('')
  })

  it('удаляет событие из списка', () => {
    const store = createStore(rootReducer)
    const wrapper = mount(
      <Provider store={store}>
        <Reminder />
      </Provider>,
    )
    const eventName = 'testEvent'
    const eventRemaining = '2030-01-01T10:30'

    expect(wrapper.contains(eventName)).toBeFalsy()

    addEvent(wrapper, eventName, eventRemaining)

    expect(wrapper.contains(eventName)).toBeTruthy()

    wrapper.find('.event button').simulate('click')

    expect(wrapper.contains(eventName)).toBeFalsy()
  })
})
