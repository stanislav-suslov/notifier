import { Background } from '../../common/event/types'

export function identifierGenerator(): () => number {
  let id = 1
  return () => id++
}

export function formatRemaining(remaining: number): string {
  if (remaining <= 0) {
    return 'наступило'
  }

  let days = 0
  let hours = 0

  while (remaining >= 1440) {
    days++
    remaining -= 1440
  }

  while (remaining >= 60) {
    hours++
    remaining -= 60
  }

  const answer: string[] = []

  days && answer.push(`${days}д`)
  hours && answer.push(`${hours}ч`)
  remaining && answer.push(`${remaining}м`)

  return answer.join(' ')
}

export function getBackground(
  minutesRemaining: number,
): Background | undefined {
  if (minutesRemaining <= 0) {
    return undefined
  }

  if (minutesRemaining < 30) {
    return Background.Red
  }

  if (minutesRemaining < 60) {
    return Background.Yellow
  }

  return Background.Green
}
