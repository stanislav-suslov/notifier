import React, { Dispatch, FunctionComponent } from 'react'
import { Event } from '../../common/event'
import { ReminderForm } from '../../common/reminder-form'
import styles from './styles.module.scss'
import {
  formatRemaining,
  getBackground,
  identifierGenerator,
} from './functions'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../../store'
import { FormActions } from '../../../store/form/types'
import { EventActions, EventId } from '../../../store/event/types'
import { EventPresenter } from '../event'

const getId = identifierGenerator()

export const Reminder: FunctionComponent = () => {
  const {
    event: { events },
    form: { name, remaining },
  } = useSelector((state: RootState) => state)

  const formDispatch = useDispatch<Dispatch<FormActions>>()
  const eventDispatch = useDispatch<Dispatch<EventActions>>()

  const setName = (payload: string) => {
    formDispatch({ type: 'SET_NAME', payload })
  }

  const setRemaining = (payload: string) => {
    formDispatch({ type: 'SET_REMAINING', payload })
  }

  const reduceRemaining = (id: EventId) => {
    eventDispatch({ type: 'REDUCE_MINUTE', payload: id })
  }

  const addEvent = () => {
    const dateNow = new Date()
    const dateRemaining = new Date(remaining)
    dateRemaining.setSeconds(dateNow.getSeconds())
    dateRemaining.setMilliseconds(dateNow.getMilliseconds())
    const minutesRemaining = (+dateRemaining - +dateNow) / 1000 / 60
    const id = getId()

    eventDispatch({
      type: 'ADD',
      payload: {
        id,
        name,
        remaining: minutesRemaining,
      },
    })

    formDispatch({ type: 'RESET' })
  }

  const removeEvent = (id: EventId) => {
    eventDispatch({ type: 'REMOVE', payload: id })
  }

  return (
    <div>
      <div className={styles.form}>
        <ReminderForm
          nameValue={name}
          nameOnChange={(e) => setName(e.target.value)}
          remainingValue={remaining}
          remainingOnChange={(e) => setRemaining(e.target.value)}
          onSubmit={addEvent}
        />
      </div>
      {events.map((event) => (
        <div className={styles.eventWrapper} key={event.id}>
          <EventPresenter
            remaining={event.remaining}
            onRemainingChange={() => reduceRemaining(event.id)}
          >
            <Event
              name={event.name}
              remaining={formatRemaining(event.remaining)}
              background={getBackground(event.remaining)}
              onRemove={() => removeEvent(event.id)}
            />
          </EventPresenter>
        </div>
      ))}
    </div>
  )
}
