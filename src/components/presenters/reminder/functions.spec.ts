import { formatRemaining } from './functions'

describe('форматирование оставшегося времени', () => {
  const minute = 1
  const hour = minute * 60
  const day = hour * 24

  it.each([
    ['наступило', -1],
    ['наступило', 0],
    ['1м', minute],
    ['10м', minute * 10],
    ['59м', minute * 59],
    ['1ч', hour],
    ['1ч 1м', hour + minute],
    ['1ч 12м', hour + minute * 12],
    ['2ч', hour * 2],
    ['1д', day],
    ['1д 1ч 1м', day + hour + minute],
    ['2д 10ч 59м', day * 2 + hour * 10 + minute * 59],
    ['33д', day * 33],
  ])(`возвращает "%s" при входном "%d"`, (expected, remaining) => {
    expect(formatRemaining(remaining)).toBe(expected)
  })
})
