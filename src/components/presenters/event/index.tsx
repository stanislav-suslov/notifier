import React from 'react'
import { Props, State } from './types'

export class EventPresenter extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      timeout: null,
    }
  }

  componentDidMount() {
    this.startTimeout()
  }

  componentWillUnmount() {
    this.clearTimeout()
  }

  onTimeoutTick() {
    if (this.props.remaining > 0) {
      this.props.onRemainingChange()
      this.startTimeout()
    } else {
      this.clearTimeout()
    }
  }

  startTimeout() {
    if (this.state.timeout) {
      clearTimeout(this.state.timeout)
    }

    const timeout = setTimeout(() => {
      this.onTimeoutTick()
    }, 60 * 1000)

    this.setState({ timeout: timeout })
  }

  clearTimeout() {
    if (this.state.timeout) {
      clearTimeout(this.state.timeout)
      this.setState({
        timeout: null,
      })
    }
  }

  render() {
    return <div>{this.props.children}</div>
  }
}
