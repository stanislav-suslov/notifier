export interface Props {
  remaining: number
  onRemainingChange: () => void
}

export interface State {
  timeout: NodeJS.Timeout | null
}
