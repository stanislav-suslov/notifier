import React from 'react'
import { FunctionComponent } from 'react'
import styles from './styles.module.scss'

export const Container: FunctionComponent = ({ children }) => (
  <div className={styles.container}>{children}</div>
)
