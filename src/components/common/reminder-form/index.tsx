import React, { FunctionComponent } from 'react'
import { Button } from '../button'
import { Background } from '../button/types'
import { FormField } from '../form-field'
import { Props } from './types'
import styles from './styles.module.scss'

export const ReminderForm: FunctionComponent<Props> = ({
  nameValue,
  nameOnChange,
  remainingValue,
  remainingOnChange,
  onSubmit,
}) => {
  function submit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault()
    onSubmit()
  }

  return (
    <form className={styles.reminder} onSubmit={submit}>
      <div>
        <FormField
          label="Событие"
          value={nameValue}
          type="text"
          required
          onChange={nameOnChange}
        />
      </div>
      <div>
        <FormField
          label="Время"
          value={remainingValue}
          required
          type="datetime-local"
          onChange={remainingOnChange}
        />
      </div>
      <div className={styles.submit}>
        <Button background={Background.Green} type="submit">
          Добавить
        </Button>
      </div>
    </form>
  )
}
