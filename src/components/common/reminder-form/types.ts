import * as FormFieldTypes from '../form-field/types'

export interface Props {
  nameValue: FormFieldTypes.Value
  nameOnChange: FormFieldTypes.onChange
  remainingValue: FormFieldTypes.Value
  remainingOnChange: FormFieldTypes.onChange
  onSubmit: () => void
}
