export interface Props {
  background: Background
  type?: 'button' | 'submit' | 'reset' | undefined
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

export enum Background {
  Green = 'green',
  Red = 'red',
}
