import React, { FunctionComponent } from 'react'
import cn from 'classnames'
import { Props } from './types'
import styles from './styles.module.scss'

export const Button: FunctionComponent<Props> = ({
  children,
  background,
  type,
  onClick,
}) => {
  const bgClass = styles[`background-${background}`]
  const classes = cn(styles.button, bgClass)

  return (
    <button className={classes} onClick={onClick} type={type}>
      {children}
    </button>
  )
}
