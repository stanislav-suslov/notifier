import React from 'react'
import { shallow } from 'enzyme'
import { FormField } from '.'

describe('поле формы', () => {
  it('устанавливает required=false, если параметр не передан явно', () => {
    const component = (
      <FormField label="" onChange={() => {}} type="text" value="" />
    )
    const wrapper = shallow(component)

    const isCorrect = wrapper.exists('input[required=false]')
    expect(isCorrect).toBeTruthy()
  })
})
