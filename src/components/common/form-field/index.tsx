import React from 'react'
import { FunctionComponent } from 'react'
import { Props } from './types'
import styles from './styles.module.scss'

export const FormField: FunctionComponent<Props> = ({
  label,
  value,
  type,
  required = false,
  onChange,
}) => (
  <div className={styles.field}>
    <div className={styles.label}>{label}</div>
    <input
      type={type}
      value={value}
      required={required}
      onChange={onChange}
      className={styles.input}
    />
  </div>
)
