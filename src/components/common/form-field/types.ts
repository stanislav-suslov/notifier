export interface Props {
  label: string
  value: Value
  required?: boolean
  type: string
  onChange: onChange
}

export type onChange = (e: React.ChangeEvent<HTMLInputElement>) => void

export type Value = string
