import React from 'react'
import { FunctionComponent } from 'react'
import { Button } from '../button'
import { Background } from '../button/types'
import { Props } from './types'
import styles from './styles.module.scss'
import cn from 'classnames'

export const Event: FunctionComponent<Props> = ({
  name,
  remaining,
  background,
  onRemove,
}) => {
  const rootClasses = cn(styles.event, {
    [styles[`background-${background}`]]: background,
  })

  return (
    <div className={rootClasses}>
      <div className={styles.remaining}>{remaining}</div>
      <div className={styles.name}>{name}</div>
      <div className={styles.remove}>
        <Button onClick={onRemove} background={Background.Red}>
          Удалить
        </Button>
      </div>
    </div>
  )
}
