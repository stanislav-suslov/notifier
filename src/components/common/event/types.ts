export interface Props {
  name: string
  remaining: string
  background?: Background
  onRemove: () => void
}

export enum Background {
  Red = 'red',
  Green = 'green',
  Yellow = 'yellow',
}
