import React from 'react'
import { Event } from './index'
import { mount, shallow } from 'enzyme'

describe('событие', () => {
  it('отображает название', () => {
    const name = 'notify name 1'

    const wrapper = shallow(
      <Event name={name} remaining={''} onRemove={() => {}} />,
    )

    expect(wrapper.contains(name)).toBeTruthy()
  })

  it('отображает оставшееся время', () => {
    const remaining = 'notify name 1'

    const wrapper = shallow(
      <Event name={''} remaining={remaining} onRemove={() => {}} />,
    )

    expect(wrapper.contains(remaining)).toBeTruthy()
  })

  it('по нажатию на кнопку "Удалить" вызывает функцию onRemove', () => {
    const onRemove = jest.fn()

    const wrapper = mount(
      <Event name={''} remaining={''} onRemove={onRemove} />,
    )

    wrapper.find('button').simulate('click')

    expect(onRemove).toHaveBeenCalledTimes(1)
  })
})
